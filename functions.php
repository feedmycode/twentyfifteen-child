<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_buddypress_style', 20 );
function enqueue_buddypress_style() {
    wp_enqueue_style( 'child-theme', get_stylesheet_directory_uri().'/buddypress.css' );
}

?>